const toggleSticky = (el) => {
    if (window.pageYOffset >= (el.offsetHeight*2)) {
        el.style.top = 0+'px'
        el.classList.add("sticky")
    } else if (window.pageYOffset < (el.offsetHeight*2) && window.pageYOffset > el.offsetHeight) {
        el.style.top = -el.offsetHeight+'px'
    } else {
        el.classList.remove("sticky")
    }
}


module.exports = {
    inserted: function (el) {
        window.onscroll = function() {
            toggleSticky(el)
        }
    }
}