import Vue from 'vue'
window.Vue = Vue

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Vuex from 'vuex'
Vue.use(Vuex)

import axios from 'axios'
window.axios = axios

window.App = {}

import FastClick from './utils/fastclick'
App.removeClickDelay = () => {
    FastClick.attach(document.body)
}

App.storage = require('store')
App.storage.addPlugin(require('store/plugins/expire'))
App.storage.addPlugin(require('store/plugins/defaults'))
App.storage.defaults({
    contactForm: {
        name: 'Patrick',
        phone: '',
        message: ''
    }
})

App.store = new Vuex.Store(require('./store/index'))

App.router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./components/Home.vue')
        },
        {
            path: '/contact',
            name: 'contact',
            component: require('./components/Contacts.vue')
        }
    ]
})

Vue.directive('sticky', require('./directives/sticky'))

App.instance = new Vue({
    el: "#app",
    store: App.store,
    router: App.router
})

App.removeClickDelay()


// styles
import './scss/styles.scss'