const removeSymbols = [ '+', '(', ')', '-', ' ', '_' ]

const cleanPhone = function (phone) {
    if (typeof phone === 'string') {
        let cleanPhone = ''
        for (let i = 0; i < phone.length; i++) {
            if (removeSymbols.indexOf(phone.charAt(i)) < 0) {
                cleanPhone += phone.charAt(i)
            }
        }
        return cleanPhone
    }
    return null
}

module.exports = cleanPhone