import axios from 'axios'

import cleanPhone from '../utils/cleanPhone'

module.exports = {
    state: {
        projects: [],
        contact: {
            sending: {
                done: false,
                message: '',
                error: false
            },
            form: App.storage.get('contactForm'),
            validationErrors: {
                name: false,
                phone: false,
                message: false
            }
        },
    },
    mutations: {
        set(state, payload) {
            state[payload[0]] = payload[1]
        },
        resetContactForm(state) {
            Vue.set(state.contact.form, 'message', '')
        },
    },
    actions: {
        getProjects(store) {
            axios.get('/api/projects.json').then(response => {
                store.commit('set', ['projects', response.data])
            })
        },
        contactSendMessage(store) {
            axios.get('/api/contacts.json').then(response => {
                let contact = { ...store.state.contact, sending: response.data }
                store.commit('set', [ 'contact', contact ])
                store.commit('resetContactForm')
                store.dispatch('contactSaveForm')
            })
        },
        contactSaveForm(store, form = false) {
            if (!form) form = store.state.contact.form
            store.commit('set', [ 'contact',  { ...store.state.contact, form: form } ])
            App.storage.set('contactForm', form)
        }
    },
    getters: {
        contactForm(state) {
            return state.contact.form
        },
        isContactFormValid(state, getters) {
            return {
                name: getters.contactForm.name.length >= 3,
                phone: cleanPhone(getters.contactForm.phone).length === 11,
                message: getters.contactForm.message.length <= 250
            }
        }
    }
}